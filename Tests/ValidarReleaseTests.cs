using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps;
using CSharpSeleniumExtentReportNetCoreTemplate.Flows;
using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using NUnit.Framework;
using System.Collections;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Tests
{
    public class ValidarReleaseTests : TestBase
    {
        #region Pages and Flows Objects
         LoginPage loginPage;
        MainPage mainPage;
        LoginFlows loginFlows;
        GerenciamentoPage gerenciamentoPage;

        #endregion

        [Test]
        public void VerificarReleaseCadastrada()
        {
            loginPage = new LoginPage();
            mainPage = new MainPage();
            loginFlows = new LoginFlows();
            gerenciamentoPage = new GerenciamentoPage();
            

            #region Parameters
            string usuario = "marcelo.silva@base2.com.br";
            string senha = "#HenriquE1#";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciar();
            gerenciamentoPage.ClicarNaOpcaoProjetos();
            gerenciamentoPage.ClicarNoProjeto();
            gerenciamentoPage.ClicarEmReleases();
        
           Assert.AreEqual("Teste de cadastro de Release", gerenciamentoPage.RetornaTextoRelease());
        }
    }
}