using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages

{
    public class GerenciamentoPage : PageBase
    {
        #region Mapping
        By textoCasoDeTeste = By.XPath("//*[text()='Cadastrar Release']");
        By reportIssueLink = By.XPath("//a[@href='/bug_report_page.php']");
        By opcaoProjetos = By.XPath("//li[4]/a/i");
        By projetoCriado = By.XPath("//mat-cell[@class='mat-cell cdk-column-identifier mat-column-identifier ng-star-inserted']");
        By opcaoCasosDeTeste = By.XPath("//div[@id='mat-tab-label-0-2']");
        By opcaoBugTracker = By.XPath("//*[text()='Bug Tracker']");
        By textoCadastrarBug = By.XPath("//*[text()='Cadastrar BUG']");
        By opcaoReleases = By.XPath("//*[text()='Releases']");
        By textoRelease = By.XPath("//*[text()='Teste de cadastro de Release']");
        By mensagemErroTextArea = By.XPath("/html/body/div[2]/font"); //exemplo de mapping incorreto
        #endregion

        
        #region Actions
        public string RetornaTextoCasoDeTesteCriado()
        {
            return GetText(textoCasoDeTeste);
        }

        public void ClicarEmReportIssue()
        {
            Click(reportIssueLink);
        }

        public void ClicarNaOpcaoProjetos()
        {
            Click(opcaoProjetos);
        }

        public void ClicarNoProjeto()
        {
            Click(projetoCriado);
        }

        public void ClicarEmCasosDeTeste()
        {
            Click(opcaoCasosDeTeste);
        }

        public void ClicarEmBugTracker()
        {
            Click(opcaoBugTracker);
        }

        public string RetornaTextoCadastrarBug()
        {
            return GetText(textoCadastrarBug);
        }

        public void ClicarEmReleases()
        {
            Click(opcaoReleases);
        }

        public string RetornaTextoRelease()
        {
            return GetText(textoRelease);
        }

        public string RetornaMensagemDeErro()
        {
            return GetText(textoRelease);
        }

        #endregion
    }
}
