﻿using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class MainPage : PageBase
    {
        #region Mapping

        By opcaoGerenciar = By.XPath("//button[text()='Gerenciar']");
        By mensagemErroTextArea = By.XPath("/html/body/div[2]/font"); //exemplo de mapping incorreto
        
        #endregion
        

        #region Actions

         public void ClicarEmGerenciar()
        {
            Click(opcaoGerenciar);
        }

 
        public string RetornaMensagemDeErro()
        {
            return GetText(mensagemErroTextArea);
        }
       
        #endregion
    }
}
